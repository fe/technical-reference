---
title: 'SUB RDD Technical Reference'
repo: '<https://gitlab.gwdg.de/fe/technical-reference>'
license: 'CC0-1.0'
authors: 'Software Quality Working Group'
version: 'x.y.z'
...
