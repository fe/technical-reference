
### Version Control

#### Policy

Developers `MUST` use `git` for version control.
Please see <https://git-scm.com/doc> for documentation.

Every repository `MUST` be copied to and synced with a version control server (VCS).

Repositories `SHOULD` follow a Git workflow.

#### Guidelines

#### Version Control Servers

Currently we use the following VCSs:

- GitLab (GWDG): <https://gitlab.gwdg.de>
- GitHub: <https://github.com/subugoe>

Which one is suitable for you depends on:

- your project
- existing code
- whether or not you want to use CI/CD or GitLab Runners
- ...

Consider mirroring of repos for project visibility (e.g. mirror GitLab code to GitHub).

#### Git Workflows

Features `SHOULD` be bundled in a separate branch before committing them to the default branch.

We recommend to use the [GitLab](https://docs.gitlab.com/ee/topics/gitlab_flow.html)/
[GitHub](https://githubflow.github.io/) Flow.

When using any kind of Flow it is good practice to protect your `main` and `develop` branch server-side.
This avoids accidental pushes to these branches.

For more information also see [Conventional Commits and Conventional Changelog](#conventional-commits-and-conventional-changelog).

##### Issue Handling

It is also `RECOMMENDED` to automatically close issues via merge request or a commit message;
How this works exactly depends on the Git repository server.
Issues can also be [referenced across repositories](https://help.github.com/articles/autolinked-references-and-urls/#commit-shas).

#### Codebase

It is `RECOMMENDED` to have only one codebase per app, see [The Twelwe-Factor App -- Codebase](https://12factor.net/codebase).
There can be many running instances of the app deployments),
which typically consists of one production site and one or more staging sites, development sites and local deployments included.

#### Further Reading

For more information on Git and related workflows, refer to the
[Atlassian tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows).
