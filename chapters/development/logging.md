
### Logging (Development)

#### Policy

##### App Responsibilities

The application is responsible for providing a stream of meaningful log messages.

 It `MUST` `NOT` determine the output destination of the log stream (e.g. write it into a log file).
 This will be handled by the environment in which the application is run.
 Instead, the application simply writes the logs to `stdout`.
 When run locally for development, the logs may be viewed directly in the terminal,
 while the test and production environment may facilitate the logs into a storage
 or analytics service for persistence and monitoring purposes.

##### Log Content

What information a single log message contains depends on the exact context (language, framework, purpose, ...).
Usually the following questions should be answered in a log message:

- WHAT happened?
- WHEN did it happen?
- WHERE did it happen?

Chose an appropriate log level and adhere to the **most common**, depending on the logging framework

- TRACE
- **DEBUG**
- **INFO**
- NOTICE
- **WARN**
- **ERROR**
- FATAL/CRITICAL

Log messages `MUST` `NOT` contain any sensitive data (access keys, passwords, personal data, ...).
If it is necessary to log these information for debugging reasons, the log level `MUST` be "DEBUG" (or lower).
A properly configured logagent will filter out all logs of level "DEBUG" (or lower) on production environments.

##### Log Style

The styling of log messages should adhere to the logging guidelines of the language you use.
Keep the language concise and consistent throughout your projects.
Good practices are:

- use present tense (e.g. *"Can not connect"* instead of *"Could not connect"*)
- start each log message with an uppercase letter (e.g. *"Can not connect"* instead of *"can not connect"*)

#### Guidelines

##### Avoid Replicating the Stacktrace with Logs

A common anti-pattern is to log an error or exception *and* pass it to the next higher caller in the stack.
Instead of this (bad) python example:

``` python
# bad example:
try:
    foo()
except Error as e:
    log.error(f"Can not do foo: {e}")
    raise e

def foo():
    try:
        bar()
    except Error as e:
        log.error(f"Can not do bar: {e}")
        raise e
```

Try to log only on the top of the caller stack and pass the error or exception up:

``` python
# good example:
try:
    foo()
except MoreConciseException as e:
    log.error("Can not do foo: %s", e)
    # error handling ...

def foo():
    try:
        bar()
    except GeneralException as e:
        raise MoreConciseException from e
```

Note that enhancing the error message is encouraged if it adds helpful information on the context of the error.
So in our example that could be:

``` python
def foo():
    try:
        bar(some_param)
    except Error as e:
        raise Exception("Can not do bar with param %s: %s" % (some_param, e))
```

##### Do not Stutter Loglevels

The loglevel chosen to create a log message is handled by the logging framework of your choice.
Avoid stuttering the loglevel in your message like

``` python
# bad example:
log.error("Error calling method foo")
```

or

``` python
# bad example:
log.info("Info: Call method bar")
```

##### Make Logs Machine Readable

For use in analytics tools, logs should be structured to be machine readable.
A common feasible format is JSON.
Depending on your logging framework you can add custom parameters to your log messages
by passing them as additional arguments to the logging method or some other means.
Avoid adding the parameters in the text like this:

``` python
# bad example:
log.error(f"Can not do bar with param {some_param} at {time.now()}")
```

And instead try passing them to the logging framework as additional arguments, e.g.:

``` python
# good example:
log.error("Can not do bar", param=some_param, timestamp=time.now())
```
