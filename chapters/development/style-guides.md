
### Style Guides

#### POLICY

You `SHOULD` adhere to established style guides for programming languages, where possible.

You `MUST` adhere to the most basic rules defined by the
[EditorConfig](http://editorconfig.org/)
[file provided with the Technical Reference source repository](https://gitlab.gwdg.de/fe/technical-reference/-/raw/main/.editorconfig).

#### Why Use Style Guides

To put it simply, style guides improve your code's readability, maintainability, and portability. Your code will be much
more understandable to others (and yourself, returning to your code after a while) if it is written in a coherent style.
A uniform style will improve code quality by facilitating debugging and review processes. It might even help you avoid
bugs in the first place by avoiding unnecessary complexity. Finally, handing over a project will be much easier when
the new as well as the old developer can quickly comprehend the code base.

#### General

Style encompasses the style of your code in the narrow sense (your functions, classes, variables, etc.), as well as that
of your documentation. Many of the style guides we use give directions on how to write code and documentation. For
languages we don't provide a recommended style guide, you `SHOULD` name and link the style guide you used in your `README`.

#### For Specific Programming Languages

For the more prominent programming languages you `SHOULD` stick to:

- **Java**: The Java style guide can be found [here](./styles/rdd-eclipse-java-google-style.xml). It's based on the
[Google style guide for Java](https://github.com/google/styleguide) with some minor RDD specific settings. You can
configure Eclipse to use it automatically at *Eclipse &gt; Preferences &gt; Java &gt; Code Style &gt; Formatter*. Just
load the [RDD Eclipse Java Google
Style](https://gitlab.gwdg.de/fe/technical-reference/-/blob/main/styles/rdd-eclipse-java-google-style.xml)
in the formatter preferences and use it in your RDD projects.

- **JavaScript**: For JS we use the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript).

- **HTML/CSS**: For HTML/CSS we agreed upon the [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html).

- **XQuery**: We use the [xqdoc style guide](http://xqdoc.org/xquery-style.pdf) with the following addenda:

  - use double quotes for easy escaping
  - prefer element constructors when creating dynamic output. Use direct notation for static content such as icons or a
  maximum of one simple XQuery expression inside: `{ some very simple query }`.
  - use minified variable names only at control variables - anything else must be *readable* and *understandable*.
  - use four spaces for a TAB (because eXide switching the preferences in eXide's setting isn't permanent)
  - for serialization to JSON prefer the `map` data type over the `json-node-output-method` ([specs](https://www.w3.org/XML/Group/qtspecs/specifications/xslt-xquery-serialization-31/html/#json-output))

- **XSLT**: Since there is no official style guide for XSLT, we decided to write
[our own](https://gitlab.gwdg.de/fe/technical-reference/tree/main/style-guides/rdd-xslt.md), resulting from
common best practices and own experiences within
the department.

- **Python**: For Python [PEP 8](https://www.python.org/dev/peps/pep-0008/) should be used, Django has a style guide
based on PEP-8 with some exceptions:
[Django-Styleguide](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/). There are
linters and tools like [flake-8](https://pypi.org/project/flake8/) and [pep-8](https://pypi.org/project/pep8/)
available as support.

- **SPARQL**: For SPARQL there is not really any official style guide and there is no possibility to simply include any
code style automatically using a code style file. We just collect some advice how to format and use SPARQL code
[here](https://gitlab.gwdg.de/fe/technical-reference/tree/main/style-guides/rdd-sparql.md).

- **Golang**: The official style guide by google can be found [here](https://google.github.io/styleguide/go/guide)
