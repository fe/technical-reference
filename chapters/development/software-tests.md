
### Software Tests

#### Policy

Software tests aim at ensuring that the code you write does exactly what you expect it to do.

All functions (except `get`ter and `set`ter methods) `MUST` provide proper tests.
Every method should have enough tests to cover each line of code, aiming at a code coverage of 100\%.
Also all possible outcomes of a method should be considered and covered by tests.

Whether you achieve this by Test Driven Development (TDD) or other means depends on your preferred way to work.

Examples for writing tests in different programming languages are:

- [**XQuery**](https://gist.github.com/joewiz/fa32be80749a69fcb8da)
