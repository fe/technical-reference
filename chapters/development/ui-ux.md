
### UI/UX Design

User Interface and User Experience are two closely related aspects of front-end development that encompass the areas of
direct interaction between user and product. User Interface generally refers to the complete space of interaction
between user and product, including the visual interface as well as the hardware periphery used for interaction.
In the context of UI/UX design, **User Interface** generally refers to the visual interface of a product,
while **User Experience** describes the interaction of the user with it. The basic aim of UI/UX design is to not
only have a usable product, but one that is pleasant to use.

This chapter presents recommendations and guidelines to improve UI and UX. For simplicity's sake, it will assume that the
relevant product is a website, although the presented content generally refers to most kinds of software products. The
first sections will cover some of the most important topics of UI/UX design, including accessibility and performance. The
final section explores how aspects of UI/UX can be tested and other ways to improve the quality of products in these areas.
At the end of the chapter, you'll find several further resources on the topics discussed.

#### UI/UX Design

This section provides some general guidelines and best practices for UI/UX design. It covers anything that does not fall
under the aspects of accessibility or performance, which are extensive enough to warrant sections of their own. For many
of the points in this section, you'll find additional resources at the end of the chapter.

##### Policy

The web-based software products we create

- `SHOULD` work on the most common browsers (Firefox, Chrome, Edge, Safari for
  both macOS and iOS) and different screen sizes (e.g. smartphone vs. desktop monitor) (**fluid design**)
- `SHOULD` be kept as simple as possible programatically (**progressive enhancement**)
- `MUST` `NOT` misdirect users unto intended actions (**dark patterns**)

#### Guidelines

- **Fluid Design**: Avoid assumptions about users' browser and viewport. One of the basic consequences of this is to
  avoid breakpoints and fixed-number dimensions if possible. You can't test for any and all browsers, so make sure a
  website works in the most common browsers (Firefox, Chrome, Edge, Safari for both macOS and iOS) and doesn't fall apart
  completely in other browsers.
  Keep in mind that it's fine for a website to look different between two browsers.
- **Progressive Enhancement**: Keep in mind the [Rule of Least Power](https://www.w3.org/2001/tag/doc/leastPower) and use
  the least powerful language available for any task whenever possible: prefer native HTML semantics over custom semantics,
  form actions over JavaScript actions, and so on.
- **Dark Patterns**: These are patterns that misdirect users into unintended actions. Avoid them. Most dark patterns apply
  more to commercial use cases than ours, but as they are prime examples of bad UX, it is useful to keep these in mind.
  See [this blog post](https://uxplaybook.org/articles/ux-dark-patterns-and-ethical-design) for some examples of dark patterns.

#### Design Systems

A design system is collection of component and pattern libraries providing a modular way to build user interfaces following
a coherent design language. They provide rules on composition, such as which colors to use, usable code for components,
layout guidelines and more. Using design systems improves efficiency and collaboration, as well as providing recognizable
repeating patterns to users, increasing the overall User Experience.

A viable design system should include as many of the following points as possible:

- **Overview and Cohesion**: The design system should contain introductory material that explains the ideas behind the
  system as a whole as well as individual parts of the system. It gives guidelines on how and when to use specific components.
  This cohesion is one main difference between a design system and a pattern or component library.
- **Design Elements**: The design system should provide color palettes, as well as icons and typography guidelines.
- **Components**: The design system should provide at least codes snippets and ideally complete components along with
  instructions on how to use these.
- **Patterns**: The design system should provide patterns ranging from distinct component groups (such as how to layout
  a form from individual selector and button components) to layout guidelines for complete web pages.
- **Content Guidelines**: The design system should provide style and content guides to ensure consistent and accessible
  text content.

##### Policy

 For projects not working with full-time designers, use of an existing design system is `RECOMMENDED`.

##### Guidelines

The following design systems are recommended because they are comprehensive, actively maintained, and widely used.
Apply one of them in your project(s) to achieve a cohesive and usable user interface.

- [Google Material Design](https://m3.material.io/)
- [Atlassian Design System](https://atlassian.design/)
- [IBM Carbon Design System](https://carbondesignsystem.com/)
- [Salesforce Lightning Design System](https://www.lightningdesignsystem.com/)
- [Shopify Polaris](https://polaris.shopify.com/)

#### Accessibility

Web accessibility (commonly abbreviated as **a11y**) refers to the creation of websites providing as few barriers that prevent
interaction to as many potential users as possible. On a technical level, this means assuring that assistive technologies
such as screen readers interact correctly with a website, that a website has functional keyboard navigation, that it
provides sufficiently visible color contrasts, and much more. But accessibility is not just opening up your website to
users with disabilities---following accessibility guidelines will improve user experience for every user. Because of this,
our team has decided to give special consideration to accessibility demands.

##### Legal Conditions

There is also a legal basis mandating the consideration of accessibility. The Web Accessibility Directive
([Directive (EU) 2016/2102](https://eur-lex.europa.eu/eli/dir/2016/2102/oj)) aims to ensure that web services of public
sector organizations implement accessibility standards. For federal institutions, the
[Barrierefreie-Informationstechnik-Verordnung](https://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) mandates
the implementation of relevant provisions according to the [Behindertengleichstellungsgesetz](https://www.gesetze-im-internet.de/bgg/BJNR146800002.html),
which in turn implements the Web Accessibility Directive for federal institutions.
At the federal state level, the [Niedersächsisches Behindertengleichstellungsgesetz](https://www.ms.niedersachsen.de/download/193359/NBGG.pdf)
[PDF] implements the Web Accessibility Directive.

Essentially, the Web Accessibility Directive mandates that public sector organizations follow W3C's Web Content
Accessibility Guidelines (WCAG) 2.0. The WCAG provide one of the most comprehensive and easily implementable set of
recommendations to achieve accessibility. Any website you work on `SHOULD` comply with these as far as possible.
The newest revision of the guidelines is the [WCAG 2.2](https://www.w3.org/TR/WCAG22/), further information can be found
in the [WCAG 2 overview](https://www.w3.org/WAI/standards-guidelines/wcag/).

Currently, [WCAG 3](https://www.w3.org/TR/wcag-3.0/) is in draft status and will eventually succeed WCAG 2.2, but,
critically, not deprecate it. Because the "WCAG 3 includes additional tests and different scoring mechanics" (Working
Draft, 24 July 2023), projects might consider referring to both WCAG 2.2 and WCAG 3 for accessibility conformance. Even
though WCAG 3 is still in draft status, a project might find it advisable to refer to its guidelines and conformance
requirements already.

Following the WCAG 2 recommendations goes a long way to make sure that your website is accessible, but modern web
applications often provide functionalities that go beyond defined HTML semantics and, therefore, have no standard-defined
way for assistive technology to interact with components, especially when interactivity comes into play. For these cases,
the [Accessible Rich Internet Applications (WAI–ARIA) 1.2](https://www.w3.org/TR/wai-aria/) recommendation provides
guidelines on how to further describe the semantics and interactions of a website, so that assistive technologies may
better interact with them. ARIA is a wide field with many nuances (and possible pitfalls that decrease accessibility
despite your best intentions).
The article [Making Sense Of WAI-ARIA](https://www.smashingmagazine.com/2022/09/wai-aria-guide/) provides a concise
introduction to ARIA, and its first rule bears repeating: Don't use ARIA. Instead, whenever
possible, use native semantics of web technologies.

Up to here, accessibility was largely discussed as a technical aspect of visual design and interactivity. Keep in mind
that accessibility is also an aspect of content design: Use plain language and avoid the use of overly technical or opaque
language whenever possible. This may not be an easy task when designing for very specific use cases and (specifically
qualified) users, but this concern of accessibility is just as important as others.

##### Policy

The software products we create `MUST` satisfy the legal requirements of the Web Accessibility Directive and the BITV.

##### Guidelines

Project `MUST` test the UI against accessibility standards criteria, ensuring that a website is as accessible in as many
use cases as
possible. These criteria are largely defined on a technical level, so it is feasible to provide extensive coverage of
the accessibility of your website through automated tests. Many of these tests run assertions that can easily be checked
during development, such as HTML attributes necessary for accessibility considerations. You should look for the
necessary plug ins to your IDE for these cases. Besides most browsers' built-in accessibility inspectors, there are also
several plug ins that provide feedback on accessibility.

But don't rely solely on tests to gauge your website's accessibility: Just because you don't see any linter/compiler/test
warnings doesn't mean your website is accessible. The UK Government Accessibility Blog published
[a case study](https://accessibility.blog.gov.uk/2017/02/24/what-we-found-when-we-tested-tools-on-the-worlds-least-accessible-webpage/)
on the performance of accessibility tools when used on a website breaking as many accessibility
rules as possible, illustrating the limits of these tools. More facetious but to the same point,
[Building the most inaccessible site possible with a perfect Lighthouse score](https://www.matuzo.at/blog/building-the-most-inaccessible-site-possible-with-a-perfect-lighthouse-score/)
describes how a completely inaccessible website can still have a near-perfect Lighthouse score. There are almost
always issues these tools don't recognize, so employ your best judgement and check for accessibility concerns yourself
often.

###### Examples of Testing Tools

- [axe-core](https://github.com/dequelabs/axe-core)
- [Pa11y](https://pa11y.org/)
- Chrome Lighthouse
- Firefox Accessibility Inspector

#### Performance

Performance is an important factor in UX design. Websites that are not performant provide a bad user experience and do
not make their content accessible. Low performance might even lead to users abandoning a website without having achieved
what they were trying to do, which is arguably the worst case outcome for any tool.

Website performance commonly includes the performance itself---simply put, the speed with which a website loads---as
well as its stability. Stability here refers to resilience---how hard is it to break something---but even more to the stability
of the layout, such as large images at the top that only render right before the website is completely loaded and cause
a shift in the position of every following element.

Performance can be increased in a number of ways, and the testing frameworks provided below usually offer suggestions to
improve performance that can easily be acted on.

Finally, keep in mind that performance is a relative measure depending on the hard- and software of a user, as well as
their internet connection. Developers tend to benefit from an above-average technical infrastructure, so what seems
sufficiently performant on your machine or during tests might not actually be as performant to the average user. There
are a number of ways to determine a relevant baseline here, but usually these are lower than one might expect, as you
can read in the article [The baseline for web development in 2022](https://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022)
about what
should be considered the technical infrastructure of a hypothetical, average international user in 2022.

As with designing for accessibilty, designing for performance is as much about empathy as it is about technical concerns.
"It works for me", is not an adequate response to performance concerns.

##### Policy

Software products `SHOULD` be as performant as possible.

##### Guidelines

Projects `SHOULD` define a performance goal and refer to a well-researched baseline when deciding on it.
The software product `SHOULD` be tested regularly by using perfomance tests.
These test the website for performance, as well as stability. This ensures a responsive website with no unnecessarily long
loading times or layout shifts that impede usability. A common benchmark for performance testing are the
[Core Web Vitals](https://web.dev/vitals/#core-web-vitals), providing measurements for *loading*, *interactivity* as well
as *visual stability*. There are a number of tools providing a suite covering a number of indicators for performance.
Try different tools and decide which one best fits a project.

###### Examples of Testing Tools

- <https://www.webpagetest.org/>
- Chrome Lighthouse
- Firefox Performance Monitor

#### Testing

Many areas of your UI/UX can be tested on a technical level: sufficient color contrast or the correct use of
`alt`-Attributes on HTML `img`-Elements can easily be ensured by automated tests. Even high level functions of a
website can be tested by methods such as end-to-end tests, ensuring that user interactions produce expected results.

But automated tests can only provide so much feedback on the functionality of your front-end. A regular regime of user
research is desirable, though often beyond the possibilities of our projects. But even the smallest project should take
some time to collect actual feedback on the user experience.

##### Policy

Software products `MUST` be tested by automated tests and `SHOULD` be tested by user research.

##### Guidelines

Keep in mind that in UI/UX design, the testing process does not just start when you push a commit to your repository and
the CI performs any defined tests. For many of the following considerations, especially accessibility, there is a number
of tools that help you improve on these areas while working on a website. These encompass browser tools and plugins, such
as Chrome's Lighthouse or Firefox's Accessibility Inspector, as well as plugins and extensions for your IDE, such as
accessibility linters for HTML or your preferred web app framework that provide feedback on your code as you write it.

User research can be as simple as asking colleages that are not involved in your project to play around with your
software product and give you feedback about the look and feel of it.
You can also give them tasks to tackle on your website and observe how easy it is for them to achieve them.
This gives you valuable feedback about how intuitive your product is designed.

A final word on test scores: some of the tools recommended below (e.g., Lighthouse) provide numerical scores for specific
tests. While using these tools as diagnostic tools is a sensible measure, using their output as scorecards is not.
If you use such tools, for example, to diagnose your merge requests, a worsened score should absolutely lead to discussion
between author and reviewer, but it should not mean that a merge request should be rejected just because a score decreased.

##### End-to-end Testing

… simulates user interaction in a browser and checks for expected outcomes. You can define a set of instructions, for example
filling a form with sample data, and check what the browser returns against the expected result. Basically, end-to-end
frameworks provide a convenient way to program a bot to "click" through a website and look out for any unexpected results.

###### Examples of Testing Tools

- [Cypress](https://www.cypress.io/)
- [Puppeteer](https://github.com/puppeteer/puppeteer)

##### Visual Regression Testing

… are a way to identify undesirable changes in layout. This becomes more important as websites get more complex, as a code
change on one page might lead to unexpected changes on another page. Visual regression testing frameworks take screenshots
of the website and compare these to earlier screenshots of successful test runs.

###### Examples of testing tools

- [Cypress](https://www.cypress.io/)
- [Percy](https://percy.io/)

##### User Research

Automated testing can only simulate user interaction so much, and good UI/UX design always needs regular user feedback.
In the context of UI/UX research, anyone can be a user, and there should not be an imaginary "ideal" user to whom a design
is catered. User feedback can come from colleagues, whether developers themselves or not, project partners, or even people
chosen more or less randomly.

You `SHOULD` conduct user research regularly during the whole development process. This does not need to be an elaborate
process. Asking one or two colleagues to try to accomplish a task on your website---or even just interact with it for a
few minutes---after a meeting is a great start to user research, providing you with a fresh set of impressions on your
work. Establish a culture of early and frequent feedback to avoid chasing impractical solutions and dead ends.

#### Resources

##### UI/UX Design

- <https://caniuse.com/>: Interactive browser support tables for front-end technologies such as HTML5, CSS3, or JS.
- <https://lawsofux.com/>: General rules and best practices to keep in mind when designing UX.
- <http://dowebsitesneedtobeexperiencedexactlythesameineverybrowser.com/>: Something to keep in mind when focussing on
  cross-browser compatibility (try opening it in different browsers!).
- <https://www.nngroup.com/articles/design-systems-101/>: A more thorough introduction to design systems.
- <https://viewports.fyi/> and
- <https://buildexcellentwebsit.es/> make excellent points for fluid designs as well as progressive enhancement.
- <https://www.kryogenix.org/code/browser/everyonehasjs.html>: Illustrates why progressive enhancement matters.
- <https://www.nngroup.com/articles/aesthetic-usability-effect/>: Shows how aesthetics and usability interact.
- <https://www.nngroup.com/articles/dark-mode-users-issues/>: Provides considerations relevant before implementing dark mode.

##### Accessibility – General

- <https://designnotes.blog.gov.uk/2017/06/29/designing-new-navigation-elements-for-gov-uk/>: Case study of an accessible
  re-design
- <https://raw.githubusercontent.com/UKHomeOffice/posters/master/accessibility/dos-donts/posters_en-UK/accessibility-posters-set.pdf>:
  Guidelines on how to design for specific user groups with accessibility needs
- <https://www.magentaa11y.com/web/>: Excellent checklists to make sure a website or components are accessible to assistive
  technologies or in non-standard views (such as high zoom levels)
- <https://developer.mozilla.org/en-US/docs/Web/Accessibility>: Entry point to accessibility
- <https://www.plainlanguage.gov/guidelines/>: United States federal guidelines on plain language
- <https://webaim.org/>: Collection of accessibility resources
- <https://www.w3.org/WAI/design-develop/>: Collection of accessibility resources

##### Accessibility – Components and Frameworks

- <https://a11y-style-guide.com/style-guide/>: Style guide and collection of accessible components
- <https://inclusive-components.design/>: Collection of accessible web native components with extensive explanations
- <https://lion-web.netlify.app/>: Collection of accessible web native components
- <https://legacy.reactjs.org/docs/accessibility.html>: Accessibility in React (deprecated)
- <https://react-spectrum.adobe.com/react-aria/index.html>: "A library of React Hooks that provides accessible UI
  primitives for your design system"
- <https://vue-a11y.com/>: Accessibility for Vue.js
