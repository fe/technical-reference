
### Explanatory Notes

**Further Reading**: We cannot cover every topic in detail, and there is fantastic existing material in many cases.
If you want to delve deeper into a topic, we provide sections providing further reading at the end of most chapters.

**CESSDA's Software Maturity Levels (SML)**: Several organizations already have put a lot of work into developing
metrics for good software. Since we don't want to reinvent the wheel, we decided to adapt (and modify if need be) a
metric which suits our needs. [CESSDA](https://www.cessda.eu/) is one of the ERICs, and although it focuses on Social
Sciences it has similar requirements regarding its software.

Throughout the document you will find sections with the heading "CESSDA's Software Maturity Levels" in which we
describe which of the SMLs we aim for and how we want to implement it.
