
### Logging (Operation)

#### Policy

##### Log Storage and Analytics

It is `RECOMMENDED` that the test and production environments
have some kind of log router configured to capture the log streams of all running services
and direct them into a warehousing and analytics system.
This enables a number of important functionalities for a project, including

- error analysis
- bugfixing
- usage statistics
- alerting
- historical analysis and traceability
