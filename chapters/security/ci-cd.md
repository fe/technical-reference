
### Continuous Integration

It is important to [separate build, release, and run stages](https://12factor.net/build-release-run) and go through
these stages frequently in order to achieve continuous integration (CI).

- In the **build stage**, the code and all its dependencies and assets are converted to a *build* bundle.
- In the **release stage**, the build is combined with a deploy [config](https://12factor.net/config).
  For example, environmental variables can be used to set the correct API endpoint for the release.
  Moreover, each release should be assigned a unique version number.
- The **run stage** refers to the actual runtime of the application.
Due to the separation of the three stages, code
cannot be changed at runtime.

We `RECOMMEND` using CI as soon as possible in new projects. Please set up your GitLab project to show your pipeline status
and test-coverage for your default branch under "Settings / General → Badges".

The workflows we are currently using in the GitLab Runner are:

- Code building
- Testing
- Code analyzer
- Packaging (JAR, WAR, DEB, XAR, Docker Image)
- Distribution (Nexus, APTLY repo, eXist repo)
- Release Management (via GitLab Environments and [GitLab flow](#version-control))
- [Generating SBOMs and uploading them to Dependency Track](#software-bill-of-materials-sbom)

#### GitLab CI

Using [GitLab for CI/CD](https://docs.gitlab.com/ee/ci/quick_start/) is `RECOMMENDED`.

#### Sample Configuration of the GitLab Runner

There is a [full and documented example](https://gitlab.gwdg.de/SADE/SADE/blob/develop/.gitlab-ci.yml) of how the
GitLab Runner is used in SADE.

### Continuous Delivery and Continuous Deployment

Continuous Delivery aims at minimizing the [gap between development and production](https://12factor.net/dev-prod-parity)
in terms of time, personnel, and tools.
This means---provided the code is integrated continuously (see above)---that code can be delivered as soon as
possible so that the difference between the delivered version and local copies remains minimal.
Each delivery SHOULD be assigned a release (see [Semantic Versioning and Semantic Release](#release-management)).

It is also important that technologies ([backing services](https://12factor.net/backing-services)) used on the
production server do not differ locally, e.g. by setting up the same database version rather
than just a simple adapter. Virtual environments such as Docker can help with this. Another way to achieve a
small gap is to build the development environment on top of the production environment.

Continuous Deployment refers to the same approach with the addition that the delivered app/code is automatically
deployed to a production environment.
