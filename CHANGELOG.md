## [2.14.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.14.0...v2.14.1) (2024-11-28)


### Bug Fixes

* add tfa-codebase reference and recommendation ([b807d76](https://gitlab.gwdg.de/fe/technical-reference/commit/b807d767ba459483deb6922a79f3123d344071ef))
* minor addition to codebase ([852e2b4](https://gitlab.gwdg.de/fe/technical-reference/commit/852e2b46bcbdca8488e653aa4044c8e3c28b7546))
* rework chapter version control ([3ad98f4](https://gitlab.gwdg.de/fe/technical-reference/commit/3ad98f41226c2288c7db61382a34d97dab058e25))

# [2.14.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.13.0...v2.14.0) (2024-01-09)


### Features

* add recommendations on docs-as-code ([01d1b29](https://gitlab.gwdg.de/fe/technical-reference/commit/01d1b29dfa56f2af871cb4bde300ec77727f45a1))

# [2.13.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.12.2...v2.13.0) (2024-01-09)


### Bug Fixes

* be more precise about Safari ([4cb828f](https://gitlab.gwdg.de/fe/technical-reference/commit/4cb828f8a43c326ee45ab1c32dbf18540d020e80))
* language, example for dark patterns ([1c81e7b](https://gitlab.gwdg.de/fe/technical-reference/commit/1c81e7bd55c13dbcd653faa9c2c7f5c86a12a25e))
* restructure according to policy/guidelines ([4ddaddb](https://gitlab.gwdg.de/fe/technical-reference/commit/4ddaddb623c8af841af4d1ff7dedf18f9c96b69b))
* update browser info ([465852f](https://gitlab.gwdg.de/fe/technical-reference/commit/465852ffa5bd6488a189d2f0eefd00f537dfb36d))


### Features

* add paragraph on WCAG 3 ([939fd20](https://gitlab.gwdg.de/fe/technical-reference/commit/939fd20bfbd9c93fc0f14322495a57cea4d813c1))
* expand chapter ui/ux ([13ff0e9](https://gitlab.gwdg.de/fe/technical-reference/commit/13ff0e9f458b06fe5e5d82e04fffd09c2d80c098))
* provide first draft of chapter ui-ux ([71b209c](https://gitlab.gwdg.de/fe/technical-reference/commit/71b209c7d7264b564bc2a95b783421890869a9c2)), closes [#114](https://gitlab.gwdg.de/fe/technical-reference/issues/114) [#63](https://gitlab.gwdg.de/fe/technical-reference/issues/63)

## [2.12.2](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.12.1...v2.12.2) (2023-09-22)


### Bug Fixes

* Final editing ([b61165f](https://gitlab.gwdg.de/fe/technical-reference/commit/b61165f1c04a614f138cf7961ad95c01f0d447d7))

## [2.12.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.12.0...v2.12.1) (2023-08-14)


### Bug Fixes

* **chapters:** transform chapters to sections; move sections to newly created chapters ([9c665d7](https://gitlab.gwdg.de/fe/technical-reference/commit/9c665d733b954c69f075c9f767173319c2836923)), closes [#123](https://gitlab.gwdg.de/fe/technical-reference/issues/123)


### Reverts

* python code snippet changes ([bdba088](https://gitlab.gwdg.de/fe/technical-reference/commit/bdba088af9678ce66250ca99d5d6450f7a6d323d))

# [2.12.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.11.0...v2.12.0) (2023-07-13)


### Features

* restructue chapter Bug Tracking ([2f52ae0](https://gitlab.gwdg.de/fe/technical-reference/commit/2f52ae0faf15831a68aa11acf110f16b4ea41792))

# [2.11.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.10.0...v2.11.0) (2023-07-12)


### Features

* refactor chapter Version Control ([13d3f7b](https://gitlab.gwdg.de/fe/technical-reference/commit/13d3f7b81b17ae584e106077dd2f90e11e2064a6))

# [2.10.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.9.1...v2.10.0) (2023-06-27)


### Features

* remove chapter on RDD onboarding ([3280ec8](https://gitlab.gwdg.de/fe/technical-reference/commit/3280ec88bfc771ffefa74cfa352f9a1ecfa71694))

## [2.9.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.9.0...v2.9.1) (2023-05-31)


### Bug Fixes

* **styleguides:** Add link to Go style guide ([7597004](https://gitlab.gwdg.de/fe/technical-reference/commit/7597004e9a16a8bd6191628f2378c21cf7b8c124))
* **styleguides:** Apply new structure ([70b5ec8](https://gitlab.gwdg.de/fe/technical-reference/commit/70b5ec8f05d7c27222574552e203038f452afb0a))

# [2.9.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.8.1...v2.9.0) (2023-05-30)


### Features

* refactor chapter Documentation ([3f058ba](https://gitlab.gwdg.de/fe/technical-reference/commit/3f058bac44ce4facfc5da1eab08850cca8489798))

## [2.8.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.8.0...v2.8.1) (2023-04-04)


### Bug Fixes

* **intellectual-property:** be more specific and avoid using "project" ([89872b9](https://gitlab.gwdg.de/fe/technical-reference/commit/89872b98562db7cf804c03b74a55cc47551dc39c)), closes [#113](https://gitlab.gwdg.de/fe/technical-reference/issues/113)

# [2.8.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.7.0...v2.8.0) (2022-11-02)


### Features

* rework "intellectual property" ([2a3a454](https://gitlab.gwdg.de/fe/technical-reference/commit/2a3a4542e215c737cad9be81cc66aeea47f1f5cd)), closes [#75](https://gitlab.gwdg.de/fe/technical-reference/issues/75)

# [2.7.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.6.0...v2.7.0) (2022-09-07)


### Bug Fixes

* **rdd.latex:** fix use of macro for single quotes in code blocks ([e432f2f](https://gitlab.gwdg.de/fe/technical-reference/commit/e432f2ffd351aa70e8bc817d4e4c61e40b5d057d))


### Features

* add sustainability links ([03dca1a](https://gitlab.gwdg.de/fe/technical-reference/commit/03dca1ab733d336f0e2d78214d60038b1e0e8daa))

# [2.6.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.5.0...v2.6.0) (2022-09-06)


### Bug Fixes

* add links to SBOM CI templates ([9f02355](https://gitlab.gwdg.de/fe/technical-reference/commit/9f02355e92bd73d8190ae86141f6fe69967e8afb))
* language ([a4dfa1f](https://gitlab.gwdg.de/fe/technical-reference/commit/a4dfa1f798a37d90c53d4f5ba666c67d9e02b851))
* links to cyclondx Java tools ([7fb7e45](https://gitlab.gwdg.de/fe/technical-reference/commit/7fb7e452b80b078f3cba22358c4ff10abec96768))


### Features

* add basic chapter for SBOMs ([6c88825](https://gitlab.gwdg.de/fe/technical-reference/commit/6c88825dd6d4c6c15d982152ade8e3d310a73749))
* add links to SBOM chapter ([b02c014](https://gitlab.gwdg.de/fe/technical-reference/commit/b02c014ab10ca432314824dbd9681a81b5f0c2ad))
* move templates to dependency track section and provide usage examples ([454484c](https://gitlab.gwdg.de/fe/technical-reference/commit/454484c9d5776f863ed21314c8f002d686291121))

# [2.5.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.4.0...v2.5.0) (2022-05-24)


### Features

* **style-guides.md:** expand on why and how to use style guides ([bee9cba](https://gitlab.gwdg.de/fe/technical-reference/commit/bee9cba4671cd8f7cca08673382437cd1d6586b7))

# [2.4.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.3.0...v2.4.0) (2022-05-18)


### Features

* add ePub ([1b6405f](https://gitlab.gwdg.de/fe/technical-reference/commit/1b6405fdf1fad40c6dfdf67e6ab101ea92414bfb))

# [2.3.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.2.1...v2.3.0) (2022-05-17)


### Bug Fixes

* **code-quality.md:** adapt MR review assignemt for a possible gitlab-ce workflow ([6ce2334](https://gitlab.gwdg.de/fe/technical-reference/commit/6ce2334fe9fa59b97d7963cc03143994a1a512fc)), closes [#74](https://gitlab.gwdg.de/fe/technical-reference/issues/74)
* **code-quality.md:** sample workflow: step 5. belongs to 4 ([862766e](https://gitlab.gwdg.de/fe/technical-reference/commit/862766e5de7d98bbf6f8bf13f8eb78a1eed6167b))
* **code-quality.md:** spelling mistake and rephrasing ([f272bc6](https://gitlab.gwdg.de/fe/technical-reference/commit/f272bc64b4629eb9f160e45905db612c24fba7f1)), closes [#142](https://gitlab.gwdg.de/fe/technical-reference/issues/142)


### Features

* **code-quality.md:** add note about code quality measurement with gitlab-ci ([158d6ac](https://gitlab.gwdg.de/fe/technical-reference/commit/158d6ac482e26d0a44ecbbe99977cd9d3b75e727)), closes [#74](https://gitlab.gwdg.de/fe/technical-reference/issues/74)

## [2.2.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.2.0...v2.2.1) (2022-04-05)


### Bug Fixes

* change gitflow to GitLab flow ([324f642](https://gitlab.gwdg.de/fe/technical-reference/commit/324f642a027343c3ec08f45874026af09b06e614))

# [2.2.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.1.1...v2.2.0) (2022-03-29)


### Features

* **onboarding.md:** add further GWDG resources to onboarding ([9df680a](https://gitlab.gwdg.de/fe/technical-reference/commit/9df680a06a6e6e7b6d50f4c6dd93595479abb2e9)), closes [#79](https://gitlab.gwdg.de/fe/technical-reference/issues/79)

## [2.1.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.1.0...v2.1.1) (2022-03-22)


### Bug Fixes

* **monitoring:** Update Icinga section ([0c9a22d](https://gitlab.gwdg.de/fe/technical-reference/commit/0c9a22d9fe292a8c645979c829a4151ae75a7d74))

# [2.1.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v2.0.0...v2.1.0) (2022-03-22)


### Features

* **document structure:** move chapter onboarding to the front ([7110f9e](https://gitlab.gwdg.de/fe/technical-reference/commit/7110f9e3ed3b842e6182344443ab0fbe91dcd8da)), closes [#78](https://gitlab.gwdg.de/fe/technical-reference/issues/78)

# [2.0.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.12.0...v2.0.0) (2022-03-22)


### Bug Fixes

* chapters/cessda-decisions.md also removed from technical-reference-main.txt ([c94f036](https://gitlab.gwdg.de/fe/technical-reference/commit/c94f03638fb5e1232c9ec818e5feb3864e0b8bdd))
* remove deprecated references to Jenkins ([1d17516](https://gitlab.gwdg.de/fe/technical-reference/commit/1d175161f4b06cc3bfce1e782a71b3b60e8d0fba))
* rephrase software coverage for clarity ([b69fa29](https://gitlab.gwdg.de/fe/technical-reference/commit/b69fa29540a5941cddafc989d689d4c276308061))


### Features

* **ci-cd.md:** recommend gitlab-ci ([c3e8a9c](https://gitlab.gwdg.de/fe/technical-reference/commit/c3e8a9c7e46b5e258397f7b53257b8c954f21d40))
* move cessda decisions to epic fe&1 ([917499d](https://gitlab.gwdg.de/fe/technical-reference/commit/917499ddd52bb1e409901e88492f09047a05286a))


### BREAKING CHANGES

* rejmove chapters/cessda-decisions.md

# [1.12.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.11.2...v1.12.0) (2022-03-15)


### Bug Fixes

* remove deprecated references to Jenkins ([aa91192](https://gitlab.gwdg.de/fe/technical-reference/commit/aa91192131dced73cf18b7bde45c07cd9af7bdd5))


### Features

* **ci-cd.md:** recommend gitlab-ci ([aa81bcd](https://gitlab.gwdg.de/fe/technical-reference/commit/aa81bcdd6ee464436c0eef8672ee0652c4f47235))

## [1.11.2](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.11.1...v1.11.2) (2022-03-10)


### Bug Fixes

* rephrase software coverage for clarity ([61c4aa8](https://gitlab.gwdg.de/fe/technical-reference/commit/61c4aa8531595ea7d78a251a5fdf3f7343ba937c))

## [1.11.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.11.0...v1.11.1) (2022-03-07)


### Bug Fixes

* give more information about lists ([cdfc842](https://gitlab.gwdg.de/fe/technical-reference/commit/cdfc842bd102fed42dd56f583c568c63a1d48693))
* restructure links; add link ([85b6af1](https://gitlab.gwdg.de/fe/technical-reference/commit/85b6af11042d8042f9b3a644fa3edd221da5fb82))

# [1.11.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.10.0...v1.11.0) (2022-03-07)


### Bug Fixes

* typo ([1b55396](https://gitlab.gwdg.de/fe/technical-reference/commit/1b55396d467a10f5042105efe7153649122ee57d))


### Features

* rank GitLab/Hub flow higher than git flow ([993ba4b](https://gitlab.gwdg.de/fe/technical-reference/commit/993ba4b252189005f030b877fe896a2129929e39))

# [1.10.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.9.3...v1.10.0) (2022-02-25)


### Bug Fixes

* semantic-release branch config ([08065b1](https://gitlab.gwdg.de/fe/technical-reference/commit/08065b14e574a27de2ecf9ff062be22cf20301ec)), closes [#84](https://gitlab.gwdg.de/fe/technical-reference/issues/84)


### Features

* add to the XQuery style guide ([64c0f6f](https://gitlab.gwdg.de/fe/technical-reference/commit/64c0f6f1b3cbc1e1251c9e8b1bedbefa5d6d2561)), closes [#83](https://gitlab.gwdg.de/fe/technical-reference/issues/83)

## [1.9.3](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.9.2...v1.9.3) (2022-02-23)


### Bug Fixes

* **style-guides:** reference this repo's editorconfig and obligate to its compliance ([24f6b60](https://gitlab.gwdg.de/fe/technical-reference/commit/24f6b60e4d757133a14c3e43b8778c5f86e8fed5))

## [1.9.2](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.9.1...v1.9.2) (2022-02-22)


### Bug Fixes

* add ENT, remove date of meeting ([0e8b11c](https://gitlab.gwdg.de/fe/technical-reference/commit/0e8b11c58961fafde5432d3557899a9a4df09108))

## [1.9.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.9.0...v1.9.1) (2022-02-22)


### Bug Fixes

* remove reference to SQWG on title page ([0fa99e3](https://gitlab.gwdg.de/fe/technical-reference/commit/0fa99e3620a77e81dd53234d7ba7cba93e42be43))

# [1.9.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.8.2...v1.9.0) (2021-11-23)


### Features

* **design-architecture.md:** designing processes for robustness and scaleability ([beebf65](https://gitlab.gwdg.de/fe/technical-reference/commit/beebf6510737dcb8d888881f14dc8c5dcc1d3a74))

## [1.8.2](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.8.1...v1.8.2) (2021-10-31)


### Bug Fixes

* **documentation:** rewrite "General Notions about Documentation" ([10d167a](https://gitlab.gwdg.de/fe/technical-reference/commit/10d167af9a2ef293dc5c92f63fd62d96d754a1e7))
* **documentation.md:** change clock from 14h to 14:00 ([a7ea37a](https://gitlab.gwdg.de/fe/technical-reference/commit/a7ea37afc25cd7a9c59829fecf9df1382e3c1395))

## [1.8.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.8.0...v1.8.1) (2021-10-19)


### Bug Fixes

* **maintenance:** add "Logging" ([725ae73](https://gitlab.gwdg.de/fe/technical-reference/commit/725ae7367cdbddf0979c521d912da536da6f86ed)), closes [#58](https://gitlab.gwdg.de/fe/technical-reference/issues/58)

# [1.8.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.5...v1.8.0) (2021-10-07)


### Features

* **maintenance.md:** add section on maintenance and administrative tasks to chapter maintenance ([0302f9f](https://gitlab.gwdg.de/fe/technical-reference/commit/0302f9fd1e99a5442b8e395a0c989837dc6ed460)), closes [#54](https://gitlab.gwdg.de/fe/technical-reference/issues/54)

## [1.7.5](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.4...v1.7.5) (2021-10-07)


### Bug Fixes

* **ci/cd:** come lint some ([91afe75](https://gitlab.gwdg.de/fe/technical-reference/commit/91afe75cac537528a370aed5f44ec6f48ced0e9f))

## [1.7.4](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.3...v1.7.4) (2021-10-05)


### Bug Fixes

* **ci-cd.md:** add twelve-factor app recommendations to CI/CD chapter ([c7af758](https://gitlab.gwdg.de/fe/technical-reference/commit/c7af75805d53f442754f734ba22c45b032b7467b))

## [1.7.3](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.2...v1.7.3) (2021-09-21)


### Bug Fixes

* Typo in about ([d64fc36](https://gitlab.gwdg.de/fe/technical-reference/commit/d64fc369d66447b343dc88f1d85369cd124272aa))

## [1.7.2](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.1...v1.7.2) (2021-07-13)


### Bug Fixes

* Add chapter III of the twelve factor app to new chapter software design ([93451b4](https://gitlab.gwdg.de/fe/technical-reference/commit/93451b42801caca28d281f66be8fcfb710d09deb))

## [1.7.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.7.0...v1.7.1) (2021-04-20)


### Bug Fixes

* replace further reading sections in new chapter structure ([f97ffea](https://gitlab.gwdg.de/fe/technical-reference/commit/f97ffeae99b33f968aeb925a9291c1a6320effb4))

# [1.7.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.6.1...v1.7.0) (2021-03-15)


### Features

* update style-guides for XQuery JSON serialization ([4451dc9](https://gitlab.gwdg.de/fe/technical-reference/commit/4451dc9cf865771fa6d7a84b28b84ec55979c706))

## [1.6.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.6.0...v1.6.1) (2021-03-09)


### Bug Fixes

* Add linter code to bash script ([18be9e0](https://gitlab.gwdg.de/fe/technical-reference/commit/18be9e0fbdc182ac91611248aca299190a9fd61c))

# [1.6.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.5.0...v1.6.0) (2021-01-12)


### Features

* markdownlint as pre-commit hook ([3d0dcb7](https://gitlab.gwdg.de/fe/technical-reference/commit/3d0dcb7d3db27f95648407618909a6f53af42d3e))

# [1.5.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.4.0...v1.5.0) (2021-01-12)


### Features

* add CC0 as license ([952e18f](https://gitlab.gwdg.de/fe/technical-reference/commit/952e18f47c2fdf23c3db8cddd9aec286bf96b809))

# [1.4.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.3.0...v1.4.0) (2020-11-05)


### Bug Fixes

* **yaml-header.md:** add variables for new latex template ([9581244](https://gitlab.gwdg.de/fe/technical-reference/commit/9581244f4748d39d823d9fc2090ff94282a8caea))
* remove surplus text passage ([d5a91ac](https://gitlab.gwdg.de/fe/technical-reference/commit/d5a91ac10b2094147a1b5cac42bd6d8e955c9ce6))


### Features

* **rdd.latex:** provide initial rework of latex-template ([a3ba794](https://gitlab.gwdg.de/fe/technical-reference/commit/a3ba7948739e1e63e990328b1939e539bdff853f)), closes [#21](https://gitlab.gwdg.de/fe/technical-reference/issues/21)
* **rdd.latex:** provide initial rework of latex-template ([2c8d5b2](https://gitlab.gwdg.de/fe/technical-reference/commit/2c8d5b234d724f794394ea12aa7d21b841f89ec3)), closes [#21](https://gitlab.gwdg.de/fe/technical-reference/issues/21)

# [1.3.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.2.0...v1.3.0) (2020-10-16)


### Bug Fixes

* **maintenance:** mark up rfc 2119 uses; add `MAY` ([61af153](https://gitlab.gwdg.de/fe/technical-reference/commit/61af153c10c1e541feaa4b4f9138d7f17d9370d2))
* broken links ([82530c0](https://gitlab.gwdg.de/fe/technical-reference/commit/82530c0117675649df750cd3261f414fea4ce9c1))
* update github links to gitlab links ([4d0628e](https://gitlab.gwdg.de/fe/technical-reference/commit/4d0628e165d10002d2c0f7008e65c74c9e2c7af7))


### Features

* **build/ci-cd/maintenance:** rearrange files and add "release management" ([6b158da](https://gitlab.gwdg.de/fe/technical-reference/commit/6b158da3954e66f0511b75e5a3e5f65df4c352bc))
* move release management to own file ([b3536e6](https://gitlab.gwdg.de/fe/technical-reference/commit/b3536e6b9e5b3202f2aaaa58e6fa158352cbd3c4))

# [1.2.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.1.1...v1.2.0) (2020-09-24)


### Features

* add terminology, minor styling and language updates ([7eff331](https://gitlab.gwdg.de/fe/technical-reference/commit/7eff33158636f09c6f579b6a49fffedf51aabc69))

## [1.1.1](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.1.0...v1.1.1) (2020-07-23)


### Bug Fixes

* Correct typos and add crlf after each sentence ([f9fcb0a](https://gitlab.gwdg.de/fe/technical-reference/commit/f9fcb0a86e8b10acccaf731bf5b8685308f55292))

# [1.1.0](https://gitlab.gwdg.de/fe/technical-reference/compare/v1.0.0...v1.1.0) (2020-06-22)


### Features

* **.markdownlint.json:** accept YAML header ([74a914c](https://gitlab.gwdg.de/fe/technical-reference/commit/74a914c741e8eb20e969bdd9a4460edee19952e6))
* add YAML header back to file ([71b57de](https://gitlab.gwdg.de/fe/technical-reference/commit/71b57de832284c2ce7fdd46401ee77b3bf696057))

# 1.0.0 (2020-06-22)


### Features

* **commitizen:** provide a customized commitizen adapter configuration ([b2645c6](https://gitlab.gwdg.de/fe/technical-reference/commit/b2645c6474eb12f4130f80d2336a3daced7e9895)), closes [#20](https://gitlab.gwdg.de/fe/technical-reference/issues/20)
* add first draft for new document structure ([d2acdbf](https://gitlab.gwdg.de/fe/technical-reference/commit/d2acdbf1f4457a672256ea2d7d8bfa91f26f7c87)), closes [#27](https://gitlab.gwdg.de/fe/technical-reference/issues/27)
* make the repo commitizen friendly ([fcb002c](https://gitlab.gwdg.de/fe/technical-reference/commit/fcb002c92a52a21958d07c775055b2d2a9aac09a)), closes [#20](https://gitlab.gwdg.de/fe/technical-reference/issues/20)
